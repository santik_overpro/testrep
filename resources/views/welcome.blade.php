<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
        $(document).ready(function(){
          $("button").click(function(){
            $.get({
              url: "/send",
              data: {
				            text: $("#text").val(),
				              },
            });
          });
        });
        </script>

		<style>

			span.time {
				opacity: .6;
			}

			span.user {
				font-weight: bold;
			}

			div.chat-input {
				    display: inline-flex;
					margin-top: 10px;
					width: 100%;
			}
		</style>
    </head>
    @include('footer')
    <body v-on:load="newFunction">
    <div id="main">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
    </ul>
    </ul>
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
        @else
            <li class="nav-item">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
  </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-top: 15px;">
<div class="card text-white bg-secondary mb-3" style="width: 100%">
  <div class="card-header">Chat</div>
  <div class="card-body">
    <div class="messages">
		    <div class="message" v-for="message in messages">
             <span class="date"
             style="color:green" v-show="message.priority == 4">
             Current date and time: @{{ message.created_at }}
             </span>
             <span class="date" v-show="message.priority !== 4">
             @{{ message.created_at }}
             </span>
                 <span class="user"
                 style="color:red" v-show="message.priority == 1">
                 @{{ message.author_name }}: (administrator)
                 </span>
                 <span class="user" v-show="message.priority == 0">
                 @{{ message.author_name }}:
                 </span>
                 <span class="user"
                 style="color:blue" v-show="message.priority === 2">
                 @{{ message.author_name }}: (moderator)
                 </span>
                 <span class="user"
                 style="color:green" v-show="message.priority === 3">
                 (System message):
                 </span>
             </span>
             <span class="text">
               @{{ message.text }}
             </span>
		    </div>
        @if (isset(Auth::user()->id))
		<div class="chat-input">
		<input type="email" id="text" class="form-control" placeholder="Message" style="width: 50%;"><button class="btn btn-primary">Send</button>
		</div>
    @endif
	</div>
  </div>
</div>
        </div>
    </div>
</div>
</div>
        <script src="/js/app.js"></script>
    </body>
</html>
