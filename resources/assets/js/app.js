
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
  el: '#main',
  data: {
     messages: messages,
   },
  methods: {
  newFunction: function(e) {

  },
}

});

Echo.channel("Messages")
.listen(".send", (e) => {
  app.messages.push({author_name: e.author, text: e.text, created_at: e.created_at, priority: e.priority});
});
