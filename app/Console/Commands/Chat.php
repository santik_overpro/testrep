<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\MessageEvent;
use App\Message;
use Carbon\Carbon;

class Chat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat:time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $message = new Message();
      $message->text = Carbon::now()->toDateTimeString();
      $message->author = 0;
      event(new MessageEvent($message));
    }
}
