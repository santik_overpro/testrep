<?php
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Controllers\MainController;
use App\Message;
use App\User;
use Auth;


class MessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $text;
    public $author;
    public $created_at;
    public $priority;

    public function __construct(Message $message)
    {
        $this->text = $message->text;
        if ((isset($message->author)) && ($message->author != 0)) {
            $user = User::find($message->author);
            $this->author = $user->name;
            $this->priority = $user->priority;
        } else {
            $this->priority = 4;
        }
        if (isset($message->created_at)) {
          $this->created_at = $message->created_at->toDateTimeString();
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('Messages');
    }

    public function broadcastAs()
    {
        return "send";
    }
}
