<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\Events\MessageEvent;
use Carbon\Carbon;
use JavaScript;
use Auth;

class MainController extends Controller
{
    public function send(Request $request) {
      $message = new Message();
      $message->text = $request->text;
      $message->author = Auth::user()->id;
      $message->save();
      event(new MessageEvent($message));
    }

    public function index() {
    /*  return view('welcome', [
         'messages' => Message::all()
      ]);*/
      $messages=
      JavaScript::put([
        'messages' => Message::take(5)
            ->join('users', 'messages.author', '=', 'users.id')
            ->select('messages.*', 'users.name as author_name', 'users.priority')
            ->orderBy('created_at', 'desc')->get()
      ]);

      return view("welcome");
    }


}
